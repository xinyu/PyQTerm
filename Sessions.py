#! /bin/python

################################################################################
#
#  Project    : PyQTerm
#  Version    : 1.0
#  Date       : Sat Dec 30 22:43:32 2017
#  Author     : Xinyu Lin
#  E-mail     : xinyu0123@gmail.com
#  WebSite    :
#  Description:
#               Qt based serial port terminal tool
#
################################################################################
import sys
import glob
import os
import serial
import serial.tools.list_ports
import time
import xml.etree.cElementTree as ET

from Ui_QTerm import TerminalTab
from datetime import datetime
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QDir, QObject, QThread, pyqtSignal, pyqtSlot
from serial import SerialException

class SessionManager():
    def __init__(self, widget, QTerm):
        self.configPath = 'config.xml'
        self.widget     = widget
        self.QTerm      = QTerm
        self.ports      = []
        self.list       = []

        self.probe()

    def probe(self):
        self.ports = []
        for (portname, desc, hwid) in (sorted(serial.tools.list_ports.comports())):
            self.ports.append(portname)

    def save(self):
        root = ET.Element("Session")

        for session in self.list:
            tab = ET.SubElement(root, "Tab", name = session.port)
            ET.SubElement(tab, "Port").text          = session.port
            ET.SubElement(tab, "BuadRate").text      = str(session.baudRate)
            ET.SubElement(tab, "DataBits").text      = str(session.dataBits)
            ET.SubElement(tab, "StopBits").text      = str(session.stopBits)
            ET.SubElement(tab, "Partiy").text        = session.parity
            ET.SubElement(tab, "FlowControl").text   = session.flowControl
            ET.SubElement(tab, "LogPath").text       = session.logPath
            ET.SubElement(tab, "SendPath").text      = session.sendPath
            ET.SubElement(tab, "ReceivePath").text   = session.receivePath
            ET.SubElement(tab, "AutoConnect").text   = 'True' if session.autoConnect else 'False'
            ET.SubElement(tab, "AutoTimestamp").text = 'True' if session.autoTimestamp else 'False'
            ET.SubElement(tab, "AutoLog").text       = 'True' if session.autoLog else 'False'
            ET.SubElement(tab, "FontFamily").text    = session.fontFamily
            ET.SubElement(tab, "FontSize").text      = str(session.fontSize)
            ET.SubElement(tab, "ColorScheme").text   = str(session.colorScheme)

        tree = ET.ElementTree(root)

        try:
            tree.write(self.configPath)
        except Exception as e1:
            raise Exception(e1)

    def load(self):
        session = None

        if os.path.exists(self.configPath):
            try:
                root = ET.parse(self.configPath).getroot()
                for tab in root.findall('Tab'):
                    session = Session(TerminalTab(self.widget, self.QTerm), self.QTerm)
                    session.port          = tab.find('Port').text
                    session.baudRate      = int(tab.find('BuadRate').text)
                    session.dataBits      = int(tab.find('DataBits').text)
                    session.stopBits      = int(tab.find('StopBits').text)
                    session.parity        = tab.find('Partiy').text
                    session.flowControl   = tab.find('FlowControl').text
                    session.logPath       = tab.find('LogPath').text
                    session.sendPath      = tab.find('SendPath').text
                    session.receivePath   = tab.find('ReceivePath').text
                    session.autoConnect   = True if tab.find('AutoConnect').text == 'True' else False
                    session.autoTimestamp = True if tab.find('AutoTimestamp').text == 'True' else False
                    session.autoLog       = True if tab.find('AutoLog').text == 'True' else False
                    session.fontFamily    = tab.find('FontFamily').text
                    session.fontSize      = int(tab.find('FontSize').text)
                    session.colorScheme   = int(tab.find('ColorScheme').text)

                    font = session.tab.te_output.font()
                    font.setFamily(session.fontFamily)
                    font.setPointSize(session.fontSize)
                    session.tab.te_output.setFont(font)
                    session.tab.le_input.setFont(font)
                    session.setColorScheme(session.colorScheme)

                    self.add(session)

                    session.setLogging(session.autoLog)
                    session.timestamp = session.autoTimestamp
                    self.widget.addTab(session.tab, session.port)

            except Exception as e1:
                raise Exception(e1)

        self.widget.currentChanged['int'].connect(self.QTerm.onTerminalTabIndexChanged)
        self.widget.tabCloseRequested['int'].connect(self.QTerm.onTerminalTabClosed)

        return session

    def add(self, session):
        if session == None:
            raise Exception('session object is not valid, unable to add it')
        else:
            self.list.append(session)

    def delete(self, session):
        if session == None:
            raise Exception('session object is not valid, unable to detele it')
        else:
            session.close()
            self.list.remove(session)

    def get(self, tab):
        for session in self.list:
            if session.tab == tab:
                return session
        return None

    def debug(self):
        for session in self.list:
            print (session.port)
            print (session.baudRate)
            print (session.dataBits)
            print (session.stopBits)
            print (session.parity)
            print (session.flowControl)
            print (session.autoConnect)
            print (session.autoTimestamp)
            print (session.autoLog)

    def close(self):
        for session in self.list:
            session.close()

        self.list = []

class Session():
    def __init__(self, tab, QTerm):
        # essentail setting for pyserial
        self.port          = '/dev/ttyUSB0'
        self.baudRate      = 921600
        self.dataBits      = 8
        self.stopBits      = 1
        self.parity        = 'N'
        self.flowControl   = 'None'
        self.timeout       = 0.1

        # directiory or file path
        self.logPath       = 'Log/ttyUSB0.log'
        self.sendPath      = QDir.homePath()
        self.receivePath   = QDir.homePath()

        self.QTerm         = QTerm
        self.tab           = tab
        self.thread        = None
        self.ser           = None
        self.fileLog       = None
        self.fontFamily    = 'DejaVu Sans Mono'
        self.fontSize      = 12
        self.colorScheme   = 1
        self.cmdHistory    = []
        self.cmdIdx        = 0

        # control flags
        self.connected     = False
        self.timestamp     = False
        self.logging       = False

        # startup settings
        self.autoConnect   = False
        self.autoTimestamp = False
        self.autoLog       = False

    def onMsgReady(self, msg):
        if self.timestamp:
            msg = str(datetime.now()) + " " + msg

        if self.logging:
            self.fileLog.write(msg+'\n')

        self.tab.te_output.append(msg)

    def onDeviceDisconnected(self, msg):
        self.disconnect()
        self.QTerm.ui.tabWidget_terminal.currentChanged.emit(0)
        self.QTerm.ui.statusBar.showMessage(msg)

    def sendMsg(self, msg):
        try:
            self.ser.write((msg + '\r').encode('utf-8'))
        except Exception as e1:
            self.disconnect()
            self.QTerm.ui.tabWidget_terminal.currentChanged.emit(0)
            self.QTerm.ui.statusBar.showMessage("error serial writing...:" + str(e1))

    def setLogging(self, state):
        self.logging = state

        if state == True:
            if self.fileLog:
                self.fileLog.close()

            self.fileLog = open(self.logPath, 'w+')
        else:
            if self.fileLog:
                self.fileLog.close()

    def setFontFamily(self, family):
        self.fontFamily = family
        font = self.tab.te_output.font()
        font.setFamily(family)
        self.tab.te_output.setFont(font)
        self.tab.le_input.setFont(font)

    def setFontSize(self, size):
        self.fontSize = size
        font = self.tab.te_output.font()
        font.setPointSize(size)
        self.tab.te_output.setFont(font)
        self.tab.le_input.setFont(font)

    def setColorScheme(self, index):
        if index == 1:
            palette = QtGui.QPalette()
            brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
            brush = QtGui.QBrush(QtGui.QColor(46, 52, 54))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
            self.tab.te_output.setPalette(palette)
            self.tab.le_input.setPalette(palette)
        elif index == 2:
            palette = QtGui.QPalette()
            brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
            brush = QtGui.QBrush(QtGui.QColor(52, 101, 164))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
            self.tab.te_output.setPalette(palette)
            self.tab.le_input.setPalette(palette)
        elif index == 3:
            palette = QtGui.QPalette()
            brush = QtGui.QBrush(QtGui.QColor(78, 154, 6))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
            brush = QtGui.QBrush(QtGui.QColor(46, 52, 54))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
            self.tab.te_output.setPalette(palette)
            self.tab.le_input.setPalette(palette)
        else:
            palette = QtGui.QPalette()
            brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
            brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
            brush.setStyle(QtCore.Qt.SolidPattern)
            palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
            palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
            self.tab.te_output.setPalette(palette)
            self.tab.le_input.setPalette(palette)

        self.colorScheme = index

    def setCmdHistory(self, cmd):
        self.cmdHistory.append(cmd)

        if len(self.cmdHistory) > 5:
            self.cmdHistory.pop(0)

        self.cmdIdx = len(self.cmdHistory)

    def getCmdHistory(self, dir):
        if len(self.cmdHistory) == 0:
            return ''

        if dir == 'prev':
            if self.cmdIdx >= 0:
                self.cmdIdx -= 1

            if self.cmdIdx == -1:
                return ''
        else:
            if self.cmdIdx < len(self.cmdHistory):
                self.cmdIdx += 1

            if self.cmdIdx == len(self.cmdHistory):
                return ''

        return self.cmdHistory[self.cmdIdx]

    def connect(self):
        try:
            self.ser = serial.Serial(port = self.port, baudrate = self.baudRate, bytesize = self.dataBits, parity = self.parity, stopbits = self.stopBits, timeout = self.timeout)
            if self.ser.isOpen():
                try:
                    self.ser.flushInput()
                    self.ser.flushOutput()

                    self.thread = SessionThread(self.ser)
                    self.thread.signal.msgReady.connect(self.onMsgReady)
                    self.thread.signal.devDisconnect.connect(self.onDeviceDisconnected)
                    self.thread.start()
                    self.connected = True

                    self.ser.write("version\r".encode('utf-8'))

                    return True
                except Exception as e1:
                    raise Exception(e1)

        except SerialException:
            raise Exception('unable to open port: %s' % self.port)

        self.disconnect()
        self.QTerm.ui.tabWidget_terminal.currentChanged.emit(0)
        return False

    def disconnect(self):
        if self.thread:
            self.thread.close()
            self.thread.quit()
            self.thread.wait()

        if self.ser:
            self.ser.close()

        self.connected = False

    def close(self):
        self.disconnect()

        if self.fileLog:
            self.fileLog.close()

class SessionSignal(QObject):
    msgReady = pyqtSignal(str)
    devDisconnect = pyqtSignal(str)

class SessionThread(QThread):
    def __init__(self, ser):
        QThread.__init__(self)
        self.ser = ser
        self.isRunning = False
        self.signal = SessionSignal()

    def __del__(self):
        self.wait()

    def run(self):
        self.isRunning = True
        try:
            while self.isRunning:
                response = self.ser.readline().strip()
                if response:
                    self.signal.msgReady.emit(response.decode("utf-8"))

        except Exception as e1:
            self.signal.devDisconnect.emit("error serial reading...: " + str(e1))

    def close(self):
        self.isRunning = False
