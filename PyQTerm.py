#! /bin/python

################################################################################
#
#  Project    : PyQTerm
#  Version    : 1.0
#  Date       : Sat Dec 30 22:43:32 2017
#  Author     : Xinyu Lin
#  E-mail     : xinyu0123@gmail.com
#  WebSite    :
#  Description:
#               Qt based serial port terminal tool
#
################################################################################

############### import modules #################################################
import sys
import glob, os
import time

from Ui_QTerm import Ui_QTerm, TerminalTab
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QFileDialog
from PyQt5.QtCore import QDir, QThread, pyqtSignal, pyqtSlot

from Sessions import Session, SessionManager

############### PyQTerm Start ##################################################
class PyQTerm(QMainWindow):
    def __init__(self, parent = None, topParent = None):
        QMainWindow.__init__(self, parent)
        self.ui = Ui_QTerm()
        self.ui.setupUi(self)
        self.session = None

        self.sm = SessionManager(self.ui.tabWidget_terminal, self)
        self.ui.cb_port.addItems(self.sm.ports)
        self.session = self.sm.load()

        if self.session:
            self.ui.tabWidget_terminal.currentChanged.emit(0)
            self.ui.actionConnect.setChecked(self.session.autoConnect)
        else:
            self.ui.actionNew.triggered.emit()

        #self.ui.tabWidget_terminal.tabBar().setTabTextColor(0, QtGui.QColor(78, 154, 6))

    def setSessionTabEnabled(self, state):
        self.ui.tab_session.setEnabled(state)
        self.ui.tab_setting.setEnabled(state)
        self.ui.actionSpeed.setEnabled(state)
        self.ui.actionTimestamp.setEnabled(state)
        self.ui.actionLog.setEnabled(state)
        self.ui.actionSend.setEnabled(state)
        self.ui.actionReceive.setEnabled(state)
        self.ui.actionRefresh.setEnabled(state)
        self.ui.actionConnect.setEnabled(state)

    def onNewActionTriggered(self):
        tab = TerminalTab(self.ui.tabWidget_terminal, self)
        self.session = Session(tab, self)
        self.sm.add(self.session)
        index = self.ui.tabWidget_terminal.addTab(tab, self.session.port)

        if self.ui.tabWidget_terminal.currentIndex != index:
            self.ui.tabWidget_terminal.setCurrentIndex(index)

        if self.ui.tabWidget_terminal.count() == 1:
            self.setSessionTabEnabled(True)

        self.ui.actionMenu.setChecked(True)
        self.ui.tabWidget_dashboard.setCurrentIndex(0)

    def onCloseActionTriggered(self):
        self.onTerminalTabClosed(self.ui.tabWidget_terminal.currentIndex())

    def onLoadActionTriggered(self):
        path, ext = QFileDialog.getOpenFileName(self, "Load File", QDir.currentPath(), "xml (*.xml)")

    def onSaveActionTriggered(self):
        self.sm.save()

    def onConnectActionToggled(self, state):
        if state == True:
            if self.session.connected == False:
                try:
                    self.session.connect()
                    self.ui.tab_session.setEnabled(False)
                    self.ui.statusBar.showMessage('%s connected' % self.session.port)
                except Exception as e:
                    self.ui.statusBar.showMessage(str(e))
                    self.ui.actionConnect.setChecked(False)
        else:
            self.session.disconnect()
            self.ui.tab_session.setEnabled(True)

    def onRefreshActionTriggered(self):
        self.sm.probe()

        self.ui.cb_port.clear()
        self.ui.cb_port.addItems(self.sm.ports)

        index = self.ui.cb_port.findText(self.session.port)
        if index != -1:
            self.ui.cb_port.setCurrentIndex(index)

    def onSendActionTriggered(self):
        path, ext = QFileDialog.getOpenFileName(self, "Load File", self.session.sendPath, "Bin (*.bin)")

    def onReceiveActionTriggered(self):
        path, ext = QFileDialog.getSaveFileName(self, "Save File", self.session.receivePath, "Bin (*.bin)")

    def onLogActionToggled(self, state):
        self.session.setLogging(state)

    def onTimestampActionToggled(self, state):
        self.session.timestamp = state

    def onSpeedActionToggled(self, state):
        if state == True:
            if self.ui.cb_baudRate.currentIndex() == 1:
                self.ui.cb_baudRate.setCurrentIndex(0)
        else:
            if self.ui.cb_baudRate.currentIndex() == 0:
                self.ui.cb_baudRate.setCurrentIndex(1)

    def onMenuActionToggled(self, state):
        if state == True:
            self.ui.tabWidget_dashboard.show()
        else:
            self.ui.tabWidget_dashboard.hide()

    def onSettingActionTriggered(self):
        if self.ui.tabWidget_dashboard.isHidden():
            self.ui.actionMenu.setChecked(True)

        self.ui.tabWidget_dashboard.setCurrentIndex(1)

    def onAboutActionTriggered(self):
        self.ui.statusBar.showMessage('PyQTerm v1.0 by Xinyu Lin')

    def onTerminalTabIndexChanged(self, index):
        self.session = self.sm.get(self.ui.tabWidget_terminal.widget(index))

        if self.session:
            self.session.tab.le_input.setFocus(QtCore.Qt.MouseFocusReason)

            index = self.ui.cb_port.findText(self.session.port)
            if index != -1:
                self.ui.cb_port.setCurrentIndex(index)

            index = self.ui.cb_baudRate.findText(str(self.session.baudRate))
            if index != -1:
                self.ui.cb_baudRate.setCurrentIndex(index)

            index = self.ui.cb_dataBits.findText(str(self.session.dataBits))
            if index != -1:
                self.ui.cb_dataBits.setCurrentIndex(index)

            index = self.ui.cb_stopBits.findText(str(self.session.stopBits))
            if index != -1:
                self.ui.cb_stopBits.setCurrentIndex(index)

            index = self.ui.cb_parity.findText(self.session.parity)
            if index != -1:
                self.ui.cb_parity.setCurrentIndex(index)

            self.ui.actionConnect.setChecked(self.session.connected)
            self.ui.actionTimestamp.setChecked(self.session.timestamp)
            self.ui.actionLog.setChecked(self.session.logging)

            self.ui.le_logPath.setText(self.session.logPath)
            self.ui.le_receivePath.setText(self.session.receivePath)
            self.ui.le_sendPath.setText(self.session.sendPath)

            self.ui.cb_autoLog.setChecked(self.session.autoLog)
            self.ui.cb_autoConnect.setChecked(self.session.autoConnect)
            self.ui.cb_autoTimestamp.setChecked(self.session.autoTimestamp)

            if self.session.connected:
                self.ui.tab_session.setEnabled(False)
            else:
                self.ui.tab_session.setEnabled(True)

            self.ui.fontComboBox.setCurrentIndex(self.ui.fontComboBox.findText(self.session.fontFamily))
            self.ui.sb_fontSize.setValue(self.session.fontSize)

    def onTerminalTabClosed(self, index):
        session = self.sm.get(self.ui.tabWidget_terminal.widget(index))
        self.sm.delete(session)

        self.ui.tabWidget_terminal.removeTab(index)
        if self.ui.tabWidget_terminal.count() == 0:
            self.setSessionTabEnabled(False)

    def onInputTextPressed(self):
        if self.session.connected:
            self.session.tab.te_output.moveCursor(QtGui.QTextCursor.End)
            self.session.sendMsg(self.session.tab.le_input.text())
            self.session.setCmdHistory(self.session.tab.le_input.text())
            self.session.tab.le_input.setText('')
        else:
            self.ui.statusBar.showMessage('%s is disconnected' % self.session.port)

    def onPortCbIndexChanged(self, string):
        # this signal will be triggered before sessions created
        if self.session:
            self.session.port = string
            self.ui.tabWidget_terminal.setTabText(self.ui.tabWidget_terminal.indexOf(self.session.tab), self.session.port)

    def onBaudRateCbIndexChanged(self, string):
        self.session.baudRate = int(string)

        if self.ui.cb_baudRate.currentIndex() == 0:
            if self.ui.actionSpeed.isChecked() == False:
                self.ui.actionSpeed.setChecked(True)
        else:
            if self.ui.actionSpeed.isChecked() == True:
                self.ui.actionSpeed.setChecked(False)

    def onDataBitsCbIndexChanged(self, string):
        self.session.stopBits = int(string)

    def onStopBitsCbIndexChanged(self, string):
        self.session.stopBits = int(string)

    def onParityCbIndexChanged(self, string):
        self.session.parity = string

    def onFlowControlCbIndexChange(self, string):
        self.session.flowControl = string

    def onLogPathTextChanged(self, string):
        self.session.logPath = string

    def onLogPathButtonClicked(self):
        _, filename = os.path.split(self.session.port)
        path, ext = QFileDialog.getSaveFileName(self, "Save File", "Log/%s.log" % filename, "Text (*.log)")
        if path != '':
            self.ui.le_logPath.setText(path)
            self.session.logPath = path

    def onSendPathButtonClicked(self):
        path = QFileDialog.getExistingDirectory(self, 'Open File', self.session.sendPath)
        if path != '':
            self.ui.le_sendPath.setText(path)
            self.session.sendPath = path

    def onReceivePathButtonClicked(self):
        path = QFileDialog.getExistingDirectory(self, 'Open File', self.session.receivePath)
        if path != '':
            self.ui.le_receivePath.setText(path)
            self.session.receivePath = path

    def onAutoConnectToggled(self, state):
        self.session.autoConnect = state

    def onAutoTimestampToggled(self, state):
        self.session.autoTimestamp = state

    def onAutoLogToggled(self, state):
        self.session.autoLog = state

    def onCurrentFontChanged(self, font):
        for session in self.sm.list:
            session.setFontFamily(font.family())

    def onFontSizeValueChanged(self, size):
        for session in self.sm.list:
            session.setFontSize(size)

    def onColorSchemeIndexChanged(self, index):
        self.session.setColorScheme(index)

    def keyPressEvent(self, event):
        if self.focusWidget() == self.session.tab.le_input:
            if event.key() == QtCore.Qt.Key_Up:
                self.session.tab.le_input.setText(self.session.getCmdHistory('prev'))
            elif event.key() == QtCore.Qt.Key_Down:
                self.session.tab.le_input.setText(self.session.getCmdHistory('next'))

    def closeEvent(self, event):
        self.sm.close()

############### PyQTerm End ####################################################

############### Main Function Start ############################################
if __name__ == '__main__':
    app = QApplication(sys.argv)
    PyQTermObj = PyQTerm()
    PyQTermObj.show()
    sys.exit(app.exec_())

############### Main Function End ##############################################
